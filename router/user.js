const express = require('express')
const router = express.Router()
const userController = require('../controller/users')

router.route('/users')
    .get(userController.get)
    .post(userController.store)

router.get('/users/create', userController.create)

router.get('/users/:id/edit', userController.edit)

router.put('/users/:id', userController.update)

router.delete('/users/:id', userController.delete)

module.exports = router