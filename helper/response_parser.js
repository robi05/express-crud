const response={
    status: false
    ,data:[]
    ,message:'Gagal menghubungkan ke server'
    ,method:null
    ,url:null
}

module.exports = {
    json_s: (req, res, data) => {
        response.status = true
        response.data = data
        response.message = 'Berhasil mendapatkan data'
        response.method = req.method
        response.url = req.url
        res.status(200).json(response)
        res.end
    }
    ,json_e: (req, res, err, extendVal = {}) => {
        response.status = false
        response.data = err
        response.message = 'Error'
        response.method = req.method
        response.url = req.url
        res.status(extendVal.status!=null?extendVal.status:500).json(response)
        res.end
    }
}