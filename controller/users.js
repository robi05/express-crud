const userModel = require('../model/users')
const response = require('../helper/response_parser')
const { v4: uuidv4 } = require('uuid');

var page = 'page/user/'

module.exports = {
    index: (req, res ) => {
        userModel.get(req.knex, (err, rows) =>{
            if (err) {
                res.render('error',{data: err})
            }else{
                const data = {
                    title:'Home'
                    ,_content:page+'index'
                    ,users:rows
                }
                res.render('app', {data})
            }
        })        
    }
    ,get : (req, res) => {        
        userModel.get(req.knex, (err, rows) =>{
            if (err) {
                response.json_e(req, res, err)
            }else{
                if (rows.length>0){
                    response.json_s(req, res, rows)
                }else{
                    response.json_e(req, res, {message: 'Data tidak ditemukan'}, {status:404})
                }
            }
        })        
    }
    ,edit : (req, res) => {
        userModel.getByWhere(req.knex, {id: req.params.id}, (err, rows) => {
            if (err){
                res.redirect('/')
            }else{
                const data = {
                    title:'Detail User'
                    ,_content:page+'edit'
                    ,users:rows
                }
                res.render('app',{data})
            }
        });
    }
    ,create : (req, res) => {
        const data = {
            title:'Tambah User'
            ,_content:page+'create'
        }
        res.render('app',{data})
    }
    ,store : (req, res) => {
        userModel.create(req.knex, {
            id: uuidv4()
            ,name: req.body.name
            ,email: req.body.email
        }, (err, result) => {
            if (err){
                res.render('error',{data: err})
            }else{
                console.log('User added', result)
                res.redirect('/')
            }
        })
    }
    ,update : (req, res) => {
        console.log("UPDATE NIH")
        userModel.update(req.knex
        ,{id: req.params.id}
        ,{
            name: req.body.name
            ,email: req.body.email
        }
        ,(err, result) => {
            if (err){
                res.render('error',{data: err})
            }else{
                console.log('User updated', result)
                res.redirect('/')
            }
        })
    }
    ,delete : (req, res) => {
        userModel.delete(req.knex
        ,{id: req.params.id}
        ,(err, result) => {
            if (err){
                res.render('error',{data: err})
            }else{
                console.log('User Deleted', result)
                res.redirect('/')
            }
        })
    }
}