const express = require('express')
const app = express()
const userRouter = require('./router/user')
const userController = require('./controller/users')
const knex = require('./config/database')
const methodOverride = require('method-override')

app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(methodOverride("_method"))

var myMiddleware = function (req, res, next) {
    req.knex = knex
    next()
}

app.use(myMiddleware)

app.set('view engine','ejs')
app.use('/assets',express.static('public'))

app.get('/', userController.index)

app.use(userRouter)

app.listen(3001, () => {
    console.log("server is ok http://localhost:3001")
});