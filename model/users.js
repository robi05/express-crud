const tableName = 'users'

module.exports = {
    get : async (con, callback) => {
        try {
            let users = await con(tableName);
            callback(null, users)
        } catch (e) {
            callback(e, null)
        }
    }
    ,getByWhere : async (con, extendWhere = {}, callback) => {
        try {
            let users = await con(tableName).where(extendWhere);
            callback(null, users)
        } catch (e) {
            callback(e, null)
        }
    }
    ,create : async (con, data = {}, callback) => {
        try{
            let insert = await con(tableName).insert(data)
            callback(null, insert[0])
        } catch (e) {
            callback(e, null)
        }
    }
    ,update : async (con, extendWhere = {}, data = {}, callback) => {
        try{
            let updated = await con(tableName).where(extendWhere).update(data)
            callback(null, updated)
        } catch (e) {
            callback(e, null)
        }
    }    
    ,delete : async (con, extendWhere = {}, callback) => {
        try{
            let deleted = await con(tableName).where(extendWhere).del()
            callback(null, deleted)
        } catch (e) {
            callback(e, null)
        }
    }

}